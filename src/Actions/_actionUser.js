import { USER_DETAIL_DRAW, USER_EDIT_DRAW, USER_DELETE_DRAW  } from '../Constants';

export const userDetailDraw = (payload) =>{
    return {
        type: USER_DETAIL_DRAW,
        payload: payload
    }
}

export const userEditDraw = (payload) =>{
    return {
        type: USER_EDIT_DRAW,
        payload: payload
    }
}

export const userDeleteDraw = (payload) =>{
    return {
        type: USER_DELETE_DRAW,
        payload: payload
    }
}