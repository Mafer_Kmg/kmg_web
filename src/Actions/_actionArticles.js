import { MODAL_VIEWARTICLES_DRAW  } from '../Constants';

export const modalViewArticlesDraw = (payload) =>{
    return {
        type: MODAL_VIEWARTICLES_DRAW,
        payload: payload,
    }
}