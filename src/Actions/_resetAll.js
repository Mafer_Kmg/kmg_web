import { RESET_ALL  } from '../Constants';

export const resetAll = (payload) =>{
    return {
        type: RESET_ALL,
        payload: payload
    }
}