import { HOME_DRAW } from "../Constants";
import { HOME_DIRECT_ACCES_DRAW } from "../Constants";
import { ARTICLES_SECTION_DRAW } from "../Constants";
import { USER_SECTION_DRAW } from "../Constants";
import { CATEGORIES_SECTION_DRAW } from "../Constants";

export const homeDraw = (payload) => {
  return {
    type: HOME_DRAW,
    payload: payload,
  };
};

export const homeDirectAccesDraw = (payload) => {
  return {
    type: HOME_DIRECT_ACCES_DRAW,
    payload: payload,
  };
};

export const articlesSectionDraw = (payload) => {
  return {
    type: ARTICLES_SECTION_DRAW,
    payload: payload,
    dataTableArticles: dataTableArticles,
  };
};

export const userSectionDraw = (payload) => {
  return {
    type: USER_SECTION_DRAW,
    payload: payload,
  };
};

export const categoriesSectionDraw = (payload) => {
  return {
    type: CATEGORIES_SECTION_DRAW,
    payload: payload,
  };
};

const dataTableArticles = {
  headers: ["Descripción", "Código", "Cantidad"],
  data: [
    {
      desc: "Loren Ipsum",
      cod: "1234",
      quant: "10",
    },
    {
      desc: "Loren Ipsum",
      cod: "1234",
      quant: "11",
    },
    {
      desc: "Loren Ipsum",
      cod: "1234",
      quant: "12",
    },
    {
      desc: "Loren Ipsum",
      cod: "1234",
      quant: "13",
    },
    {
      desc: "Loren Ipsum",
      cod: "1234",
      quant: "14",
    },
  ],
};
