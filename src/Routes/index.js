import React from "react";
import { Switch } from "react-router-dom";
import Route from "./Route";
import { ThemeProvider } from '@material-ui/styles';

//COMPONENTES
import Login from "../Components/Login";
import Home from "../Components/Home";

//CONSTANTES
import { ADMI_THEME } from '../Constants';

const theme = ADMI_THEME;

export default function Routes() {
  return (
      <ThemeProvider theme={theme}>
        <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/home" component={Home} isPrivate />
        </Switch>
      </ThemeProvider>
  );
}
