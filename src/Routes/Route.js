// Configura la logica del enrutamiento.

import React from "react";
import { Route, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import Login from "../Components/Login/";
import Home from "../Components/Home/";

export default function RouteWrapper({
  component: Component,
  isPrivate,
  authent,
  ...rest
}) {
  var signed = false;

  if (localStorage.getItem("signed") !== null) {
    signed = true;
  }

  if (isPrivate && !signed) {
    return <Login to="/" />;
  }

  if (!isPrivate && signed) {
    return <Home to="/home" />;
  }

  if (authent === true) {
    return <Home to="/home" />;
  }

  const Layout = signed ? Home : Login;

  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}