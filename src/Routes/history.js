// Gestiona el historico de las rutas relativas.

import { createBrowserHistory } from "history";

export default createBrowserHistory();
