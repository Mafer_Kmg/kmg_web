import { createMuiTheme } from "@material-ui/core/styles";

export const ADMI_THEME = createMuiTheme({

  palette: {
    primary: {
      main: '#391157',
    },
    secondary: {
      main: '#ede7f6',
    },
  },
  typography: {
    button: {
      textTransform: 'none'
    }
  }

});

/* Reducer Constants */
export const RESET_ALL = 'RESET_ALL';

export const HOME_DRAW = 'HOME_DRAW';

export const HOME_DIRECT_ACCES_DRAW = 'HOME_DIRECT_ACCES_DRAW';

export const ARTICLES_SECTION_DRAW = 'ARTICLES_SECTION_DRAW';

export const MODAL_VIEWARTICLES_DRAW = 'MODAL_VIEWARTICLES_DRAW';

export const USER_SECTION_DRAW = 'USER_SECTION_DRAW';

export const USER_DETAIL_DRAW = 'USER_DETAIL_DRAW';

export const USER_EDIT_DRAW = 'USER_EDIT_DRAW';

export const USER_DELETE_DRAW = 'USER_DELETE_DRAW';

export const CATEGORIES_SECTION_DRAW = 'CATEGORIES_SECTION_DRAW';

