import { USER_DETAIL_DRAW, USER_EDIT_DRAW, USER_DELETE_DRAW  } from '../Constants';

export const userReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_DETAIL_DRAW:
      return { ...state, userDetailPayload: action.payload };
    case USER_EDIT_DRAW:
      return { ...state, userEditPayload: action.payload };
    case USER_DELETE_DRAW:
      return { ...state, userDeletePayload: action.payload };
    default:
      return state;
  }
};
