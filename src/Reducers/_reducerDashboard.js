import { HOME_DRAW } from "../Constants";
import { HOME_DIRECT_ACCES_DRAW } from "../Constants";
import { ARTICLES_SECTION_DRAW } from "../Constants";
import { USER_SECTION_DRAW } from "../Constants";
import { CATEGORIES_SECTION_DRAW} from "../Constants";

export const dashboardReducer = (state = {}, action) => {
  switch (action.type) {
    case HOME_DRAW:
      return { ...state, homePayload: action.payload };
    case HOME_DIRECT_ACCES_DRAW:
      return { ...state, directAccesPayload: action.payload };
    case ARTICLES_SECTION_DRAW:
      return { ...state, articlesSectionPayload: action.payload, dataTableArticles: action.dataTableArticles };
    case USER_SECTION_DRAW:
      return { ...state, userSectionPayload: action.payload };
    case CATEGORIES_SECTION_DRAW:
      return { ...state, categoriesSectionPayload: action.payload };

    default:
      return state;
  }
};
