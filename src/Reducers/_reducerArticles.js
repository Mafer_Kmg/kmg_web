import { MODAL_VIEWARTICLES_DRAW } from "../Constants";

export const articlesReducer = (state = {}, action) => {
  switch (action.type) {
    case MODAL_VIEWARTICLES_DRAW:
      return { ...state, 
        drawModalViewArticles: action.payload, 
      };

    default:
      return state;
  }
};
