import { combineReducers } from "redux";
import { resetAll } from './_resetAll';
import { dashboardReducer } from './_reducerDashboard';
import { userReducer } from './_reducerUser';
import { articlesReducer } from './_reducerArticles';


import { RESET_ALL } from '../Constants'

const appReducer = combineReducers({
  resetAll,
  dashboardReducer,
  userReducer,
  articlesReducer,
});

const resetState = combineReducers({
  resetAll,
  dashboardReducer,
  userReducer,
  articlesReducer
});


const rootReducer = (state, action) => {
  if (action.type === RESET_ALL) {
      state = undefined
      return resetState(state, action)
  } else {
      return appReducer(state, action)
  }
}
export default rootReducer;
