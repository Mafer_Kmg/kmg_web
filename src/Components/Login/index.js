/* Dependencias  */
import React, { Component } from "react";
import { connect } from "react-redux";
import Slide from "@material-ui/core/Slide";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";

/* Actions */

/* Constants and styles */
import "./index.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.functionConnect = this.functionConnect.bind(this);
  }

  functionConnect(id) {
    // switch (id) {
    //   case "User":
    //     var nombre = "mfernanda";
    //     if (nombre === "") {
    //       alert("Todos los campos son obligatorios. ");
    //     } else {
    //       this.setState({ nombre: true });
    //     }
    //     break;

    //   case "Password":
    //     var password = "M1234";
    //     if (password === "") {
    //       alert("Todos los campos son obligatorios. ");
    //     } else {
    //       this.setState({ password: true });
    //     }
    //     break;
    //   default:
    //     break;
    // }
    localStorage.setItem("signed", true);
    window.location.reload(); //recarga la pagina.
  }

  onChangeValues = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <Grid container direction="row" className="body">
        <Slide direction="right" in={true}>
          <Grid item xs={12} sm={5} md={6} className="modal">
            <Grid
              container
              spacing={0}
              alignItems="center"
              justify="center"
              direction="center"
            >
              <Avatar className="avatar">
                <LockOutlinedIcon />
              </Avatar>
              <Grid item xs={12}>
                <div className="title">
                  <Typography variant="h5" className="Letter">
                    INICIA SESIÓN
                  </Typography>
                </div>
              </Grid>
              <form className="form" autoComplete="off">
                <Grid item xs={12}>
                  <TextField
                    className="campoColor"
                    id="User"
                    name="Username"
                    onChange={(event) => {
                      this.onChangeValues(event);
                    }}
                    value={this.state.Username}
                    label={<Typography>Nombre de usuario</Typography>}
                    variant="outlined"
                    margin="normal"
                    width=""
                    placeholder="Escriba su usuario"
                  />
                </Grid>
                <Grid
                  direction="column"
                  alignItems="center"
                  justify="center"
                  item
                  xs={12}
                  sm={12}
                  md={12}
                >
                  <TextField
                    className="campoColor"
                    id="Password"
                    onKeyUp={() => {
                      this.functionConnect("Password");
                    }}
                    label={<Typography>Contraseña</Typography>}
                    variant="outlined"
                    margin="normal"
                    //fullWidth
                    name="Contraseña"
                    placeholder="Escriba su contraseña"
                  />
                </Grid>

                <Grid item xs={12}>
                  <Button
                    type="submit"
                    // fullWidth
                    variant="contained"
                    className="submit"
                    onClick={this.functionConnect}
                  >
                    INGRESAR
                  </Button>
                </Grid>

                <Grid item xs={12}>
                  <img
                    alt="GUSSADMI"
                    src="https://firebasestorage.googleapis.com/v0/b/guss-web.appspot.com/o/componentes%2Fgusslogo-white.png?alt=media&token=6fe8858a-0190-4e69-ade2-74c077b7656f "
                    className="logo"
                  />
                </Grid>
              </form>
            </Grid>
          </Grid>
        </Slide>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(Login);
