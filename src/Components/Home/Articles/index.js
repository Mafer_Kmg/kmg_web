//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { Typography, Container, Paper, Button } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { withStyles, Divider } from "@material-ui/core";
import SearchOutlinedIcon from "@material-ui/icons/SearchOutlined";
import CancelSharpIcon from "@material-ui/icons/CancelSharp";
import TextField from "@material-ui/core/TextField";
import VisibilityIcon from "@material-ui/icons/Visibility";
import CreateIcon from "@material-ui/icons/Create";
import CancelIcon from "@material-ui/icons/Cancel";

/*COMPONENTES */
import DirectAccess from "../DirectAccess";
import ViewArticles from "../Articles/ViewArticles";
import { ButtonHook } from "../ButtonHook";
import { InputHook } from "../../InputHook";
import { TableHook } from "../TableHook";

/*ACTIONS */
import {
  homeDraw,
  homeDirectAccesDraw,
  articlesSectionDraw,
} from "../../../Actions/_actionDashboard";
import { modalViewArticlesDraw } from "../../../Actions/_actionArticles";

/*CONSTANTS */
import { styles } from "./constants";

class Articles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: 44,
      tabla: this.props.dataTableArticles,
    };
  }

  addArticles = () => {
    this.props.modalViewArticlesDraw(true);
  };

  alerta = () => {
    alert("sad");
  };

  onChangeValue = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({ [name]: value });
  };
  funcion = () => {
    this.setState({ searchArticle: "" });
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        <Grid container spacing={2} className={classes.root}>
          <Grid item lg={10} md={10} sm={10}>
            <Typography variant="h4">Artículos</Typography>
          </Grid>
          <Grid item lg={2} md={2} sm={2} className={classes.buttonAddArticles}>
            <ButtonHook
              variant="contained"
              color="primary"
              functionOnclick={this.addArticles}
              name="Agregar Artículos"
              icon="add_circle"
              // typographyVariant="caption"
              size="large"
            />
          </Grid>
          <Grid item lg={12} md={12} sm={12}>
            <DirectAccess />
          </Grid>
          <Grid item lg={12} md={12} sm={12} className={classes.searchInput}>
            <InputHook
              name="searchArticle"
              value={this.state.searchArticle}
              fullWidth={true}
              helperText=""
              label="Búsqueda"
              variant="outlined"
              type="string"
              startIcon={<SearchOutlinedIcon color="primary" />}
              endIcon={<CancelSharpIcon color="primary" fontSize="small" />}
              endIconFunction={this.funcion}
              onChangeInput={this.onChangeValue}
            />
          </Grid>
          <Grid item lg={12} md={12} sm={12} className={classes.table}>
            <TableHook
              tableData={this.state.tabla}
              columnActionsName="Acciones"
              ActionAdd={
                <ButtonHook
                  variant="contained"
                  color="primary"
                  functionOnclick={this.addArticles}
                  icon={<VisibilityIcon />}
                  size="medium"
                />
              }
              ActionEdit={
                <ButtonHook
                  variant="contained"
                  color="primary"
                  functionOnclick={this.addArticles}
                  icon={<CreateIcon />}
                  size="medium"
                />
              }
              ActionDelete={
                <ButtonHook
                  variant="contained"
                  color="primary"
                  functionOnclick={this.addArticles}
                  icon={<CancelIcon />}
                  size="medium"
                />
              }
            />
          </Grid>
        </Grid>
        {this.props.drawModalViewArticles && <ViewArticles />}
      </>
    );
  }
}

const articles = withStyles(styles, { withTheme: true })(Articles);

function mapStateToProps(state) {
  return {
    drawModalViewArticles: state.articlesReducer.drawModalViewArticles,
    dataTableArticles: state.dashboardReducer.dataTableArticles,
  };
}

export default connect(mapStateToProps, {
  homeDraw,
  homeDirectAccesDraw,
  articlesSectionDraw,
  modalViewArticlesDraw,
})(articles);
