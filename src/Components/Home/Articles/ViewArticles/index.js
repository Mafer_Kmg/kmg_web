//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles, Divider } from "@material-ui/core";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from "@material-ui/core/Grid";
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';


//ACTIONS
import { modalViewArticlesDraw } from "../../../../Actions/_actionArticles";

/*CONSTANTS */
import { styles } from "./constants";

class ViewArticles extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  closeModal = () => {
    this.props.modalViewArticlesDraw(false)
  }

  render() {
    const { classes } = this.props;
    return (
        <>
            <Dialog
                open={true}
                fullWidth={true}
                maxWidth='lg'
                scroll='body'
                // onClose={}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                className={classes.modalStyle1}
            >
                {/* <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle> */}
                <DialogContent>
                    <div className={classes.root}>
                        <Grid container spacing={3}>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Paper elevation={3} className={classes.mainSection}></Paper>
                            </Grid>
                            <Grid item xs>
                                <Grid container spacing={1} className={classes.secondSection}>
                                    <Grid item xs={6} className={classes.secondImage}>
                                        <Paper elevation={3}  className={classes.paper}></Paper>
                                    </Grid>
                                    <Grid item xs={6} className={classes.secondImage}>
                                        <Paper elevation={3}  className={classes.paper}></Paper>
                                    </Grid>
                                    <Grid item xs={6} className={classes.secondImage}>
                                        <Paper elevation={3}  className={classes.paper}></Paper>
                                    </Grid>
                                    <Grid item xs={6} className={classes.secondImage}>
                                        <Paper elevation={3}  className={classes.paper}></Paper>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs>
                                <Grid container spacing={6}>
                                    <Grid item xs={12}>
                                        <Paper elevation={3} className={classes.paper} style={{padding: '1em'}}></Paper>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Paper elevation={3} className={classes.paper} style={{padding: '1em'}}></Paper>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Paper elevation={3} className={classes.paper} style={{padding: '1em'}}></Paper>
                                    </Grid>
                                </Grid>
                                
                            </Grid>
                        </Grid>
                    </div>
                </DialogContent>
                <DialogActions>
                <Button color="primary" onClick={this.closeModal}>
                    Disagree
                </Button>
                <Button color="primary" autoFocus onClick={this.closeModal}>
                    Agree
                </Button>
                </DialogActions>
            </Dialog>
        </>
    );
  }
}

const viewArticles = withStyles(styles, { withTheme: true })(ViewArticles);

function mapStateToProps(state) {
  return {};
}

export default connect(
    mapStateToProps, {
        modalViewArticlesDraw
    }
)(viewArticles);

