const drawerWidth = 240;

export const styles = (theme) => ({
    root: {
        flexGrow: 1,
        height: '41em',
      },
      mainSection: {
        height: '24em',
        textAlign: 'center',
        color: theme.palette.text.secondary,
        // border: '2px dashed grey',
        backgroundColor: '#ede7f6'
      },
      secondSection: {
        height: '24em',
        // textAlign: 'center',
        color: theme.palette.text.secondary,
      },
      secondImage: {
        // padding: theme.spacing(0.5),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
      paper: {
        // padding: theme.spacing(2),
        height: '100%',
        textAlign: 'center',
        color: theme.palette.text.secondary,
        // border: '2px dashed grey',
        backgroundColor: '#ede7f6'
      },

});