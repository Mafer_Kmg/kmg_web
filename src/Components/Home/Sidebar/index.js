import React, { Component } from "react";
import { connect } from "react-redux";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import LayersIcon from "@material-ui/icons/Layers";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import HomeIcon from "@material-ui/icons/Home";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import CategoryIcon from "@material-ui/icons/Category";
import BookmarksIcon from "@material-ui/icons/Bookmarks";
import SubjectIcon from "@material-ui/icons/Subject";
import { styles } from "../../../";
import { withStyles, Divider } from "@material-ui/core";

//ACTIONS
import {
  homeDraw,
  homeDirectAccesDraw,
  articlesSectionDraw,
  userSectionDraw,
  categoriesSectionDraw,
} from "../../../Actions/_actionDashboard";
import { resetAll } from "../../../Actions/_resetAll";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.clickMenu();
  };

  clickMenu = (option) => {
    this.props.resetAll();

    switch (option) {
      case "Inicio":
        this.props.homeDraw(true);
        this.props.homeDirectAccesDraw(true);
        this.props.articlesSectionDraw(false);
        break;
      case "Articulos":
        this.props.homeDraw(false);
        this.props.homeDirectAccesDraw(false);
        this.props.articlesSectionDraw(true);
        break;
      case "Usuarios":
        this.props.userSectionDraw(true);
        break;
      case "Categorias":
        this.props.categoriesSectionDraw(true);
        break;
      default:
        this.props.homeDraw(true);
        this.props.homeDirectAccesDraw(true);
        break;
    }
  };

  /**
   * DEBE CAMBIARSE LA FORMA EN QUE SE TRABAJA A FUTURO
   * ES SOLO UNA BASE PARA PODER MONTAR LA ESTRUCTURA Y ENTENDER CÓMO FUNCIONA REACTJS
   */
  render() {
    return (
      <>
        <ListItem
          button
          onClick={() => {
            this.clickMenu("Inicio");
          }}
        >
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText primary="Inicio" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Articulos");
          }}
        >
          <ListItemIcon>
            <LocalMallIcon />
          </ListItemIcon>
          <ListItemText primary="Artículos" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Usuarios");
          }}
        >
          <ListItemIcon>
            <AccountCircleIcon />
          </ListItemIcon>
          <ListItemText primary="Usuarios" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Categorias");
          }}
        >
          <ListItemIcon>
            <CategoryIcon />
          </ListItemIcon>
          <ListItemText primary="Categorias" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Ofertas");
          }}
        >
          <ListItemIcon>
            <AddShoppingCartIcon />
          </ListItemIcon>
          <ListItemText primary="Ofertas" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Marcas");
          }}
        >
          <ListItemIcon>
            <BookmarksIcon />
          </ListItemIcon>
          <ListItemText primary="Marcas" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Secciones");
          }}
        >
          <ListItemIcon>
            <LayersIcon />
          </ListItemIcon>
          <ListItemText primary="Secciones" />
        </ListItem>

        <ListItem
          button
          onClick={() => {
            this.clickMenu("Materiales");
          }}
        >
          <ListItemIcon>
            <SubjectIcon />
          </ListItemIcon>
          <ListItemText primary="Materiales" />
        </ListItem>
      </>
    );
  }
}

// const sidebar = withStyles(styles, { withTheme: true })(Sidebar);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {
  homeDraw,
  homeDirectAccesDraw,
  articlesSectionDraw,
  userSectionDraw,
  categoriesSectionDraw,
  resetAll,
})(Sidebar);
