/*DEPENDENCIAS*/
import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import SearchOutlinedIcon from "@material-ui/icons/SearchOutlined";
import CancelSharpIcon from "@material-ui/icons/CancelSharp";

/*COMPONENTES*/
import DirectAccess from "../DirectAccess";
import { ButtonHook } from "../ButtonHook";
import { InputHook } from "../../InputHook";
import { TableHook } from "../TableHook";

/*ACTIONS*/
import {
  homeDirectAccesDraw,
  categoriesSectionDraw,
} from "../../../Actions/_actionDashboard";

/*CONSTANTS*/
import { styles } from "./constants";

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  /*FUNCIONES*/

  render() {
    const { classes } = this.props;
    return (
      <>
        <Grid container spacing={2}>
          <Grid item sm={10} md={10} lg={10}>
            <Typography variant="h4">Categorías</Typography>
          </Grid>
          <Grid item sm={2} md={2} lg={2}>
            <ButtonHook
              variant="contained"
              color="primary"
              name="Agregar Categoría"
              icon="add_circle"
              size="large"
            />
          </Grid>
          <Grid item sm={12} md={12} lg={12}>
            <DirectAccess />
          </Grid>
          <Grid item sm={12} md={12} lg={12}>
            <InputHook
              name="searchArticle"
              fullWidth={true}
              helperText=""
              label="Búsqueda"
              variant="outlined"
              type="string"
              startIcon={<SearchOutlinedIcon color="primary" />}
              endIcon={<CancelSharpIcon color="primary" fontSize="small" />}
            />
          </Grid>
          <Grid item sm={12} md={12} lg={12} >
              <TableHook>

              </TableHook>
          </Grid>
        </Grid>
      </>
    );
  }
}

const categories = withStyles(styles, { withTheme: true })(Categories);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {
  homeDirectAccesDraw,
  categoriesSectionDraw,
})(categories);
