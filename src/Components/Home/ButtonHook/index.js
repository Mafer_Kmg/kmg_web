import React, { useState, useEffect } from 'react';
import {Button,Typography} from "@material-ui/core";
import Icon from '@material-ui/core/Icon';
import { useStyles } from "./styles";

export function ButtonHook(statesProps) {
    
    const [props, setProps] = useState(statesProps);
    const classes = useStyles()();

    useEffect(() => {
        setProps(statesProps)
    });

    // const updateField = e => {
    //     setState({
    //       ...form,
    //       [e.target.name]: e.target.value
    //     });
    // };

    return (
        <div>
            <Button 
                variant={props.variant} 
                color={props.color} 
                size={props.size} 
                onClick={props.functionOnclick}
                disabled={props.disabled}
            >
                {props.icon!==undefined&&
                    <Icon className={classes.iconStyle}>{props.icon}</Icon>
                }
                {props.name!==undefined&&
                    <Typography variant='caption' className={classes.textStyle}>
                        {props.name}
                    </Typography>
                }
            </Button>
        </div>
  );
}