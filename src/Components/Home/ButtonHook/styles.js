import { makeStyles } from '@material-ui/core';

export const useStyles = props => makeStyles( theme => ({
    textStyle:{
        margin: theme.spacing(0, -1, 0, 1)
    },
    iconStyle: {
        padding: theme.spacing(0, 0.4, 0, 0),
        margin: theme.spacing(0, -1, 0, -1)
    }
}));