const drawerWidth = 240;

export const styles = theme => ({

container: {
  marginTop: 30, 
}, 
image:{
  width: theme.spacing(30),
  height: theme.spacing(20),
  marginTop: 30,
  padding: theme.spacing(2, 4, 2, 4),
  

},
text:{
  marginBottom: 10,
  padding: theme.spacing(2, 4, 2, 4),
},
title:{
  marginLeft: 20,
  marginBottom: 30,
  marginTop: 15, 
  
},
Icon:{
  marginLeft: 50,
},
subtitle:{
  marginBottom: 25,
},




})