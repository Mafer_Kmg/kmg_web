/*DEPENDENCIAS*/
import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {
  Typography,
  Paper,
} from "@material-ui/core";
import { styles } from "./constants";
import IconButton from "@material-ui/core/IconButton";
import TodayIcon from "@material-ui/icons/Today";


/*COMPONENTES*/

/*ACTIONS*/

/*CONSTANTS*/

class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  /*FUNCIONES*/

  render() {
    const { classes } = this.props;

    return (
      <>
        <Grid container spacing={2} className={classes.container}>
          {/* DRAWER LEFT */}
          <Grid item xs={12} sm={6}>
            <Paper variant="outlined">
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <img
                    alt="GUSSADMI"
                    src="https://firebasestorage.googleapis.com/v0/b/guss-muebles-web.appspot.com/o/componentes%2Fgusslogo.png?alt=media&token=addd1cf6-08a6-4aab-86d6-fc3bbe84bec4 "
                    className={classes.image}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="body2" className={classes.text}>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. Lorem Ipsum is simply dummy text of the
                    printing and typesetting industry.
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          {/* DRAWER RIGHT*/}
          <Grid item xs={12} sm={6}>
            <Paper variant="outlined">
              <Grid container>
                <Grid item xs={12}>
                  <Typography variant="h3" className={classes.title}>
                    Heading
                  </Typography>
                </Grid>
                <Grid container spacing={0}>
                  <Grid item xs={3} sm={3}>
                    <IconButton>
                      <TodayIcon fontSize="large" className={classes.Icon} />
                    </IconButton>
                  </Grid>
                  <Grid item xs={9} sm={9}>
                    <Typography variant="subtitle2">First Headline</Typography>
                    <Typography variant="body1" className={classes.subtitle}>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry.
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={0}>
                  <Grid item xs={3} sm={3}>
                    <IconButton>
                      <TodayIcon fontSize="large" className={classes.Icon} />
                    </IconButton>
                  </Grid>
                  <Grid item xs={9} sm={9}>
                    <Typography variant="subtitle2">First Headline</Typography>
                    <Typography variant="body1" className={classes.subtitle}>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry.
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={0}>
                  <Grid item xs={3} sm={3}>
                    <IconButton>
                      <TodayIcon fontSize="large" className={classes.Icon} />
                    </IconButton>
                  </Grid>
                  <Grid item xs={9} sm={9}>
                    <Typography variant="subtitle2">First Headline</Typography>
                    <Typography variant="body1" className={classes.subtitle}>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry.
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </>
    );
  }
}

const info = withStyles(styles, { withTheme: true })(Info);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(info);
