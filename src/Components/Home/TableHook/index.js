import React, { useState, useEffect } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { StyledTableCell, StyledTableRow, useStyles } from "./constants";

export function TableHook(statesProps) {

    const [props, setProps] = useState(statesProps);
    const [headers, setHeaders] = useState(props.tableData.headers);
    const [data, setData] = useState(props.tableData.data);
    const [keys, setKeys] = useState(Object.keys(props.tableData.data[0]))
    const classes = useStyles();

    useEffect(() => {
        setProps(statesProps)
    })  

    return (
        <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
            <TableHead>
            <TableRow>
                {headers.map((element, i) => (
                    <StyledTableCell key={'head'+i} component="th" scope="row" align="center">{element}</StyledTableCell>
                ))}
                {props.columnActionsName!==undefined&&
                    <StyledTableCell key={props.columnActionsName} component="th" scope="row" align="center">{props.columnActionsName}</StyledTableCell>
                }
            </TableRow>
            </TableHead>
            <TableBody>
            {data.map((data, index) => (
                <>
                    <StyledTableRow key={data+index}>
                        {keys.map((keys, i) => (
                            <StyledTableCell key={data[keys]+index+i} component="th" scope="row" align="center">
                                {Array.isArray(data[keys])?
                                    <>
                                        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                                            {data[keys].map((icons, inx) => (
                                                <div style={{marginLeft: '1%'}}>
                                                    {icons}
                                                </div>
                                            ))}
                                        </div>
                                    </>
                                    :
                                    <>
                                        {data[keys]}
                                    </>
                                }
                            </StyledTableCell>
                        ))}
                        {props.columnActionsName!==undefined&&
                            <StyledTableCell>
                                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                                    {props.ActionAdd!==undefined&&
                                        <div style={{marginLeft: '1%'}}>
                                            {props.ActionAdd}
                                        </div>
                                    }  
                                    {props.ActionEdit!==undefined&&     
                                        <div style={{marginLeft: '1%'}}>
                                            {props.ActionEdit}
                                        </div>
                                    }
                                    {props.ActionDelete!==undefined&&
                                        <div style={{marginLeft: '1%'}}>
                                            {props.ActionDelete}
                                        </div>
                                    }
                                </div>
                                
                            </StyledTableCell>
                        }
                    </StyledTableRow>
                </>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
    );
}