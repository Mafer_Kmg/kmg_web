import { colors } from "@material-ui/core";

const drawerWidth = 240;

export const styles = (theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    background: "#391157",
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },

  burgerButton: {
    margininRight: theme.spacing(1), // margen entre el logo y el burgen button
    width: 120,
  },
  hide: {
    display: "none",
  },
  toolbar1: theme.mixins.toolbar, //alto del appbar
  logo: {
    width: "100px",
    height: "40px",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(10, 1, 1, 2),
  },
  account: {
    width: "300px",
    flexGrow: 1,
  },

  color: {
    background: "#2c87d7",
  },
});
