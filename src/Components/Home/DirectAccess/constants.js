const drawerWidth = 240;

export const styles = (theme) => ({
  drawer: {
    backgroundColor: "#ede7f6",
  },
  directAcces: {
    padding: theme.spacing(0,2,0,0),
  },
  firstDirectAcces: {
    padding: theme.spacing(0,2,0,0),
  },
  lastDirectAcces: {
    padding: theme.spacing(0,1,0,1),
  },
  directAccesNumber: {
    padding: theme.spacing(2,0,0,4),
    fontSize: 14
  },
  Icon:{
    color: "#391157",
    
  },
  directAccesIcon:{
    color: "#f06292",
    justifyContent: "right",
  },

  title:{
    color:"#212121",
    marginLeft: 10,

  },








});
