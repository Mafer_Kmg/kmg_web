/*DEPENDENCIAS*/
import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import VisibilityIcon from "@material-ui/icons/Visibility";
import NearMeIcon from "@material-ui/icons/NearMe";
import {
  Paper,
  Button,
  Typography,
  Divider,
  Container,
} from "@material-ui/core";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import IconButton from "@material-ui/core/IconButton";
import { styles } from "./constants";
import { withStyles } from "@material-ui/core/styles";

/*COMPONENTES*/

/*ACTIONS*/

/*CONSTANTS*/

class Access extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  /*FUNCIONES*/

  render() {
    const { classes } = this.props;

    return (
      <>
        <Grid container spacing={1}> 
          {/* DRAWER UNO*/}
          <Grid item xs={6} md={3} className={classes.directAcces}>
            <Paper elevation={4}>
              <Grid container className={classes.drawer}>
                <Grid item md={4}>
                  <IconButton>
                    <VisibilityIcon
                      color="primary"
                      fontSize="large"
                      className={classes.Icon}
                    />
                  </IconButton>
                </Grid>
                <Grid item md={8}>
                  <Container className={classes.directAccesNumber}>
                    <Typography> 44 </Typography>
                  </Container>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.drawer}>
                <Divider variant="middle"></Divider>
                <Button className={classes.directAccesIcon}>
                  <NearMeIcon />
                  <Typography className={classes.title}>
                    Artículos Activos
                  </Typography>
                </Button>
              </Grid>
            </Paper>
          </Grid>
          {/*  DRAWER TWO*/}
          <Grid item xs={6} md={3} className={classes.directAcces}>
            <Paper elevation={4}>
              <Grid container spacing={0} className={classes.drawer}>
                <Grid item md={4}>
                  <IconButton>
                    <VisibilityOffIcon
                      className={classes.Icon}
                      fontSize="large"
                    />
                  </IconButton>
                </Grid>
                <Grid item md={8}>
                  <Container className={classes.directAccesNumber}>
                    <Typography> 44 </Typography>
                  </Container>
                </Grid>
              </Grid>

              <Grid item xs={12} className={classes.drawer}>
                <Divider variant="middle"></Divider>
                <Button className={classes.directAccesIcon}>
                  <NearMeIcon />
                  <Typography className={classes.title}>
                    Artículos Inactivos
                  </Typography>
                </Button>
              </Grid>
            </Paper>
          </Grid>
          {/*  DRAWER THREE*/}
          <Grid item xs={6} md={3} className={classes.directAcces}>
            <Paper elevation={4}>
              <Grid container spacing={0} className={classes.drawer}>
                <Grid item md={4}>
                  <IconButton>
                    <ShoppingCartIcon
                      className={classes.Icon}
                      fontSize="large"
                    />
                  </IconButton>
                </Grid>
                <Grid item md={8}>
                  <Container className={classes.directAccesNumber}>
                    <Typography> 13 </Typography>
                  </Container>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.drawer}>
                <Divider variant="middle"></Divider>
                <Button className={classes.directAccesIcon}>
                  <NearMeIcon />
                  <Typography className={classes.title}>
                    Artículos en Oferta
                  </Typography>
                </Button>
              </Grid>
            </Paper>
          </Grid>
          {/*  DRAWER FOUR*/}
          <Grid item xs={6} md={3} className={classes.directAcces}>
            <Paper elevation={4}>
              <Grid container className={classes.drawer}>
                <Grid item md={4}>
                  <IconButton>
                    <VisibilityIcon fontSize="large" className={classes.Icon} />
                  </IconButton>
                </Grid>
                <Grid item md={8}>
                  <Container className={classes.directAccesNumber}>
                    <Typography> 44 </Typography>
                  </Container>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.drawer}>
                <Divider variant="middle"></Divider>
                <Button className={classes.directAccesIcon}>
                  <NearMeIcon />
                  <Typography className={classes.title}>
                    Comentarios
                  </Typography>
                </Button>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </>
    );
  }
}

const access = withStyles(styles, { withTheme: true })(Access);

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, {})(access);
