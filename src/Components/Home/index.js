//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { styles } from "./constants";
import Grid from "@material-ui/core/Grid";
import Drawer from "@material-ui/core/Drawer";
import { withStyles, Divider } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Sidebar from "./Sidebar";
import clsx from "clsx";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { Link } from "@material-ui/core";

/*COMPONENTES */
import DirectAccess from "../Home/DirectAccess";
import Information from "../Home/Information";
import Articles from "../Home/Articles";
import User from "../Home/User";
import UserEdit from "../Home/User/UserEdit";

/*ACTIONS */
import {
  homeDraw,
  homeDirectAccesDraw,
  articlesSectionDraw,
  userSectionDraw,
  categoriesSectionDraw,
} from "../../Actions/_actionDashboard";

import { userEditDraw } from "../../Actions/_actionUser";

/*CONSTANTS */
import { ADMI_THEME } from "../../Constants";
import Categories from "./Categories";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    };
  }

  /**
   * @description Se encarga de aperturar el burgenMenu
   * @param open
   * @return true
   */
  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  /**
   * @description Se encarga de cerrar el burgenMenu
   * @param open
   * @return false
   */
  handleDrawerClose = () => {
    this.setState({
      open: false,
    });
  };

  logout = () => {
    localStorage.removeItem("signed");
    window.location.reload(); //recarga la pagina.
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        <div className={this.state.open ? classes.root : ""}>
          {/* NAVBAR */}
          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: this.state.open,
            })}
          >
            <Toolbar>
              <Grid container spacing={0}>
                <Grid item xs={1} md={1}>
                  <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="open drewer"
                    onClick={this.handleDrawerOpen}
                    className={clsx(this.state.open && classes.hide)}
                  >
                    <MenuIcon />
                  </IconButton>
                </Grid>
                <Grid item xs={9} md={10}>
                  <div style={{ textAlign: "center" }}>
                    <Link underline="none" href="#" color="inherit">
                      <img
                        alt="GUSSADMI"
                        src="https://firebasestorage.googleapis.com/v0/b/guss-web.appspot.com/o/componentes%2Fgusslogo-white.png?alt=media&token=6fe8858a-0190-4e69-ade2-74c077b7656f "
                        className={classes.logo}
                      />
                    </Link>
                  </div>
                </Grid>
                <Grid item xs={2} md={1}>
                  <div style={{ textAlign: "center" }}>
                    <IconButton
                      color="inherit"
                      onClick={this.logout}
                      // className={clsx(this.state.open && classes.hide)}
                    >
                      <AccountCircleIcon />
                    </IconButton>
                  </div>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>

          {/* SIDEBAR */}
          <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            classes={{
              paper: classes.drawerPaper,
            }}
            open={this.state.open}
          >
            <div className={classes.drawerHeader}>
              <IconButton onClick={this.handleDrawerClose}>
                {ADMI_THEME.direction === "rtl" ? (
                  <ChevronRightIcon />
                ) : (
                  <ChevronRightIcon />
                )}
              </IconButton>
            </div>
            <Divider />
            <Sidebar />
          </Drawer>

          {/* CONTENIDO */}
          <main className={classes.content}>
            {this.props.directAccesPayload && <DirectAccess />}
            {this.props.homePayload && <Information />}
            {this.props.articlesSectionPayload && <Articles />}
            {this.props.userSectionPayload && <User />}
            {this.props.userEditPayload && <UserEdit />}
            {this.props.categoriesSectionPayload && <Categories/>}
          </main>
        </div>
      </>
    );
  }
}

const home = withStyles(styles, { withTheme: true })(Home);

function mapStateToProps(state) {
  return {
    homePayload: state.dashboardReducer.homePayload,
    directAccesPayload: state.dashboardReducer.directAccesPayload,
    articlesSectionPayload: state.dashboardReducer.articlesSectionPayload,
    userSectionPayload: state.dashboardReducer.userSectionPayload,
    userEditPayload: state.userReducer.userEditPayload,
    categoriesSectionPayload: state.dashboardReducer.categoriesSectionPayload,
  };
}

export default connect(mapStateToProps, {
  homeDraw,
  homeDirectAccesDraw,
  articlesSectionDraw,
  userSectionDraw,
  userEditDraw,
  categoriesSectionDraw,
})(home);
