//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import VisibilityIcon from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

/*COMPONENTES */

/*ACTIONS */
import {
  userDetailDraw,
  userEditDraw,
  userDeleteDraw,
} from "../../../../Actions/_actionUser";
import { userSectionDraw } from '../../../../Actions/_actionDashboard';

/*CONSTANTS */
import { styles } from "./constants";

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  /**
   *
   * @param {*} id Recibe el id del registro de la columna seleccionada
   * @param {*} operacion Recibe el tipo de operación a realizar 0 -> Ver Usuario, 1 -> Editar Usuario, 2 -> Borrar Usuario
   */
  gestionUser = (id, operacion) => {

    switch (operacion) {
      case 1 /* Editar Usuario */:
        this.props.userEditDraw(true);
        this.props.userSectionDraw(false);
        break;
      case 2 /* Borrar Usuario */:

        this.props.userDeleteDraw(true);
        break;
      default:
        /* Ver Usuario */
        this.props.userDetailDraw(true);
        break;
    }
  };

  render() {
    const { classes } = this.props;
    const rows = [
      {
        id: 0,
        name: "Ismar Tovar",
        rol: "Super Administrado",
        status: "Activo",
      },
      {
        id: 1,
        name: "María Monagas",
        rol: "Super Administrado",
        status: "Activo",
      },
      {
        id: 2,
        name: "Alexander López",
        rol: "Super Administrado",
        status: "Activo",
      },
      {
        id: 3,
        name: "Yesenia Apolinar",
        rol: "Cliente",
        status: "Activo",
      },
      {
        id: 4,
        name: "Gonzalo Apolinar",
        rol: "Administrador",
        status: "Inactivo",
      },
    ];
    return (
      <>
        <Grid item md={12} className={classes.tablePaddingList}>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Nombre</TableCell>
                  <TableCell align="center">Permisos</TableCell>
                  <TableCell align="center">Estatus</TableCell>
                  <TableCell align="center">Acciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row" align="center">
                      {row.name}
                    </TableCell>
                    <TableCell align="center">{row.rol}</TableCell>
                    <TableCell align="center">{row.status}</TableCell>
                    <TableCell align="center">
                      <Button
                        onClick={() => {
                          this.gestionUser(row.id, 0);
                        }}
                      >
                        <VisibilityIcon />
                      </Button>
                      <Button
                        onClick={() => {
                          this.gestionUser(row.id, 1);
                        }}
                      >
                        <EditIcon />
                      </Button>
                      <Button
                        onClick={() => {
                          this.gestionUser(row.id, 2);
                        }}
                      >
                        <DeleteIcon />
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </>
    );
  }
}

const userList = withStyles(styles, { withTheme: true })(UserList);

function mapStateToProps(state) {
  return {
    
  };
}

export default connect(mapStateToProps, {
  userDetailDraw,
  userEditDraw,
  userDeleteDraw,
  userSectionDraw
})(userList);
