//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles, Divider, Typography } from "@material-ui/core";
import List from "@material-ui/core/List";
import { Link } from "@material-ui/core";

/*COMPONENTES */
import DirectAccess from "../DirectAccess";
import UserList from "./UserList";
import ModalDetail from "./UserDetail";
import UserEdit from "./UserEdit";
import ModalDelete from "./UseDelete";

/*ACTIONS */
import {
  userDetailDraw,
  userDeleteDraw,
} from "../../../Actions/_actionUser";

/*CONSTANTS */
import { styles } from "./constants";

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;
    return (
      <>
        <Typography className={classes.paddingTitle} variant="h4">
          Usuarios
        </Typography>
        <DirectAccess />
        <UserList />

        {this.props.userVerPayload && <ModalDetail />}
        {this.props.userDeletePayload && <ModalDelete />}
      </>
    );
  }
}

const user = withStyles(styles, { withTheme: true })(User);

function mapStateToProps(state) {
  return {
    userVerPayload: state.userReducer.userDetailPayload,
    userDeletePayload: state.userReducer.userDeletePayload,
  };
}

export default connect(mapStateToProps, {
  userDetailDraw,
  userDeleteDraw,
})(user);
