//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";

/*COMPONENTES */

/*ACTIONS */
import {
  userDetailDraw,
  userEditDraw,
  userDeleteDraw,
} from "../../../../Actions/_actionUser";

/*CONSTANTS */
import { styles } from "./constants";

class ModalDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClose = () => {
    this.props.userDetailDraw(false);
  };

  render() {
    const { classes } = this.props;
    return (
      <Dialog
        open={this.props.modalPayload}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Detalles del Usuario"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means sending
            anonymous location data to Google, even when no apps are running.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const modalDetail = withStyles(styles, { withTheme: true })(ModalDetail);

function mapStateToProps(state) {
  return {
    modalPayload: state.userReducer.userDetailPayload,
  };
}

export default connect(mapStateToProps, {
  userDetailDraw,
  userEditDraw,
  userDeleteDraw,
})(modalDetail);
