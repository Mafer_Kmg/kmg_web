//DEPENDENCIAS
import React, { Component } from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";

/*COMPONENTES */

/*ACTIONS */
import {
  userDetailDraw,
  userEditDraw,
  userDeleteDraw,
} from "../../../../Actions/_actionUser";

/*CONSTANTS */
import { styles } from "./constants";

class UserEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClose = () => {
    this.props.userEditDraw(false);
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        <Typography variant="h4"> Editar Usuario</Typography>
        <form noValidate autoComplete="off">
          <Grid item xs={12} sm={12} md={12}>
            <Paper elevation={3} className={classes.p1}>
              <Typography color="primary">Datos del titular</Typography>
              <Grid container xs={12} sm={12} md={12}>
                <Grid item xs={12} sm={12} md={6} style={{ padding: "5px" }}>
                  <TextField
                    required
                    style={{ marginTop: "10px !important" }}
                    fullWidth
                    name="CiFieldNumber"
                    margin="dense"
                    label="Cédula de identidad"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} style={{ padding: "5px" }}>
                  <TextField
                    required
                    style={{ marginTop: "10px !important" }}
                    fullWidth
                    name="NameFieldText"
                    margin="dense"
                    label="Nombre"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} style={{ padding: "5px" }}>
                  <TextField
                    required
                    style={{ marginTop: "10px !important" }}
                    fullWidth
                    type="text"
                    name="SurnameFieldText"
                    margin="dense"
                    label="Apellido"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} style={{ padding: "5px" }}>
                  <TextField
                    required
                    select
                    fullWidth
                    name="GenderFieldSelect"
                    margin="dense"
                    variant="outlined"
                    id="outlined-select-currency"
                    label="Sexo"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} style={{ padding: "5px" }}>
                  <TextField
                    required
                    style={{ marginTop: "10px !important" }}
                    fullWidth
                    type="text"
                    name="DateField"
                    margin="dense"
                    label="Fecha"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </form>
      </>
    );
  }
}

const userEdit = withStyles(styles, { withTheme: true })(UserEdit);

function mapStateToProps(state) {
  return {
    modalPayload: state.userReducer.userEditPayload,
  };
}

export default connect(mapStateToProps, {
  userDetailDraw,
  userEditDraw,
  userDeleteDraw,
})(userEdit);
