import React, { useState, useEffect } from 'react';
import {Button,Typography} from "@material-ui/core";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';



// import { useStyles } from "./styles";

export function InputHook(statesProps) {
    
    const [props, setProps] = useState(statesProps);
    // const { padding } = useStyles(props)();

    useEffect(() => {
        setProps(statesProps)
    });

    // const updateField = e => {
    //     setState({
    //       ...form,
    //       [e.target.name]: e.target.value
    //     });
    // };

    return (
        <div>
            <TextField
                color="primary"
                value={props.value}
                name={props.name}
                required={props.required}                                   //True-False
                fullWidth={props.fullWidth}                                 //True-False
                error={props.error}                                         //True-False
                helperText={props.helperText!==undefined&&props.helperText} //Text error
                label={props.label}                                         //Nombre del campo
                variant={props.variant}                                     //opciones: filled y outlined
                type={props.type}                                           //opciones: password, number y string
                defaultValue={props.defaultValue}                           //Texto por defecto
                size={props.size}                                           //opciones: small y normal
                margin={props.margin}                                       //opciones: dense y normal
                onChange={props.onChangeInput}                              //ejecuta función enviada al ser seteado o borrado        
                InputProps={props.startIcon!==undefined?            
                    {          
                        readOnly: props.readOnly,                               //True-false
                        startAdornment:                                     //Icono inicial
                        (               
                            <InputAdornment position="start"  style={{ marginLeft: '-12px' }}>
                                {props.startIcon&&
                                    <Button onClick={props.startIconFunction}>
                                        {props.startIcon}
                                    </Button>
                                }
                            </InputAdornment>
                        ),
                        endAdornment:                                           //Icono final
                        (                          
                            <InputAdornment position="start" style={{ marginRight: '-12px' }}>
                                {props.endIcon&&
                                    <Button onClick={props.endIconFunction}>
                                        {props.endIcon}
                                    </Button>
                                }
                            </InputAdornment>
                        )
                    }
                :
 
                    {          
                        readOnly: props.readOnly,
                        endAdornment:                                           //Icono final
                        (                          
                            <InputAdornment position="start" style={{ marginRight: '-12px' }}>
                                {props.endIcon&&
                                    <Button onClick={props.endIconFunction}>
                                        {props.endIcon}
                                    </Button>
                                }
                            </InputAdornment>
                        )
                    }
     
                }   
            />
        </div>
  );
}